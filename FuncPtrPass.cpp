#include "FuncPtrPass.h"

using namespace llvm;

char FuncPtrPass::ID = 0;
static RegisterPass<FuncPtrPass> X("funcptrpass", "Print function call instruction");

inline bool isFunctionPtrType(const Type *type) {
    if (auto ptr_type = dyn_cast<PointerType>(type)) {
        auto type = ptr_type->getElementType();
        if (type->isFunctionTy()) {
            return true;
        }
    }
    return false;
}

inline bool isFunctionPtrTypeWithFnRetTy(const Type *type) {
    if (auto ptr_type = dyn_cast<PointerType>(type)) {
        auto type = ptr_type->getElementType();
        if (auto fnty = dyn_cast<FunctionType>(type)) {
            return isFunctionPtrType(fnty->getReturnType());
        }
    }
    return false;
}

#ifdef DEBUG_OUTPUT
inline auto &debugPrintVar(const Value *var) {
    if (var->hasName()) {
        debug() << var->getName();
    } else {
        debug() << "<no name>{";
        var->print(debug());
        debug() << "}";
    }
    return debug();
}
#else
inline auto debugPrintVar(const Value *var) {
    return debug();
}
#endif

bool FuncPtrPass::runOnModule(Module &module) {
    initialFpVals(module);
    while (!unsolved.empty()) {
        while (!unsolved.empty()) {
            /* FOR DEBUG MESSAGE OUTPUT
            for (auto &kv : dependencies) {
                if (kv.first->getName() == "t_fptr") {
                    debug() << "t_fptr have dependencies: ";
                    for (auto v : kv.second) {
                        debug() << v->getName() << ", ";
                    }
                    debug() << "\n";
                }
            }
            */
            auto beg = unsolved.begin();
            auto var = *beg;
            unsolved.erase(beg);
            debug() << "find unsolved: ";
            debugPrintVar(var) << '\n';
            update(var);
        }
        while (!changed.empty()) {
            auto it = changed.begin();
            auto var = *it;
            changed.erase(it);
            debug() << "find changed: ";
            debugPrintVar(var) << '\n';
            onChanged(var);
        }
    }
    outputMsg();
    return false;
}

void FuncPtrPass::onChanged(const Value *var) {
    if (fn_with_fn_args.find(var) != fn_with_fn_args.end()) {
        onChangedFnWithFnArgs(var);
    }
    if (isFunctionPtrTypeWithFnRetTy(var->getType())) {
        for (auto user : var->users()) {
            if (auto call_inst = dyn_cast<CallBase>(user)) {
                if (call_inst->getCalledValue() == var) {
                    unsolved.insert(call_inst);
                }
            }
        }
    }
    for (auto related : dependent_on[var]) {
        unsolved.insert(related);
    }
}

void FuncPtrPass::onChangedFnWithFnArgs(const Value *var) {
    for (auto user : var->users()) {
        if (auto call_inst = dyn_cast<CallBase>(user)) {
            if (call_inst->getCalledValue() == var) {
                for (auto i = 0; i < call_inst->arg_size(); i++) {
                    auto argi = call_inst->getArgOperand(i);
                    if (auto argi_fn = dyn_cast<Function>(argi)) {
                        for (auto callee_fn : possible_values[var]) {
                            auto formal_argi = callee_fn->getArg(i);
                            newVar(formal_argi);
                            updatePossibleValue(formal_argi, argi_fn);
                        }
                    } else if (isFunctionPtrType(argi->getType())) {
                        for (auto callee_fn : possible_values[var]) {
                            addDependency(callee_fn->getArg(i), argi);
                        }
                    }
                }
            }
        }
    }
}

void FuncPtrPass::initialFpValsHelp(const Instruction &inst) {
    if (isa<DbgInfoIntrinsic>(inst))
        return; // not considering instrinsic debug functions
    if (auto call_inst = llvm::dyn_cast<llvm::CallBase>(&inst)) {
        const auto *callee = call_inst->getCalledValue();
        if (auto callee_fn = dyn_cast<Function>(callee)) {
            for (auto i = 0; i < callee_fn->arg_size(); i++) {
                auto argi = callee_fn->getArg(i);
                if (isFunctionPtrType(argi->getType())) {
                    newVar(argi);
                    auto actual_argi = call_inst->getArgOperand(i);
                    if (auto actual_argi_fn = dyn_cast<Function>(actual_argi)) {
                        updatePossibleValue(argi, actual_argi_fn);
                    } else {
                        addDependency(argi, call_inst->getArgOperand(i));
                    }
                }
            }
        } else {
            newVar(callee);
            for (auto &arg_use : call_inst->args()) {
                auto arg = arg_use.get();
                if (isa<Function>(arg)) {
                    fn_with_fn_args.insert(callee);
                } else if (isFunctionPtrType(arg->getType())) {
                    newVar(arg);
                    fn_with_fn_args.insert(callee);
                }
            }
        }
        call_insts.push_back(call_inst);
    }
}

void FuncPtrPass::initialFpVals(const Module &module) {
    for (const auto &fn : module) { // TODO: initial of global vals may also call functions
        for (const auto &bb : fn) {
            for (const auto &inst : bb) {
                initialFpValsHelp(inst);
            }
        }
    }
}

void FuncPtrPass::addDependencies(const Value *var) {
    FValSet &fvals = possible_values[var];
    if (auto phi = dyn_cast<PHINode>(var)) {
        // PHI
        for (const auto &use : phi->incoming_values()) {
            auto binded_var = use.get();
            if (const auto fn = dyn_cast<Function>(binded_var)) {
                updatePossibleValue(var, fvals, fn);
            } else if (!isa<ConstantPointerNull>(binded_var)) {
                addDependency(var, binded_var);
            }
        }
    } else if (auto select = dyn_cast<SelectInst>(var)) {
        // SELECT
        auto true_var = select->getTrueValue();
        if (const auto fn = dyn_cast<Function>(true_var)) {
            updatePossibleValue(var, fvals, fn);
        } else if (!isa<ConstantPointerNull>(true_var)) {
            addDependency(var, true_var);
        }
        auto false_var = select->getFalseValue();
        if (const auto fn = dyn_cast<Function>(false_var)) {
            updatePossibleValue(var, fvals, fn);
        } else if (!isa<ConstantPointerNull>(false_var)) {
            addDependency(var, false_var);
        }
    } else if (auto call = dyn_cast<CallBase>(var)) {
        // Call
        auto callee = call->getCalledValue();
        if (auto callee_fn = dyn_cast<Function>(callee)) {
            addDependency(var, callee_fn);
        } else {
            newVar(callee);
            for (auto callee_fn : possible_values[callee]) {
                addDependency(var, callee_fn);
            }
        }
    } else if (const auto arg = dyn_cast<Argument>(var)) {
        // nothing to do, the arguments' dependency should be added otherwhere
    } else if (const auto func = dyn_cast<Function>(var)) {
        // solve the return value func
        for (auto &bb : *func) {
            for (auto &inst : bb) {
                if (auto ret_inst = dyn_cast<ReturnInst>(&inst)) {
                    addDependency(var, ret_inst);
                }
            }
        }
    } else if (const auto ret_inst = dyn_cast<ReturnInst>(var)) {
        // the return value
        auto ret_value = ret_inst->getReturnValue();
        if (const auto fn = dyn_cast<Function>(ret_value)) {
            updatePossibleValue(var, fvals, fn);
        } else if (!isa<ConstantPointerNull>(ret_value)) {
            addDependency(var, ret_value);
        }
    } else {
        errs() << "!!!UNCONSIDERED FUNC VAL: ";
        var->print(errs());
        errs() << '\n';
    }
}

void FuncPtrPass::update(const Value *var) {
    FValSet &fvals = possible_values[var];
    for (auto d : dependencies[var]) {
        bindValues(var, fvals, d);
    }
}

void FuncPtrPass::bindValues(const Value *var, FValSet &val_set, const Value *dependency) {
    for (auto fn : possible_values[dependency]) {
        updatePossibleValue(var, val_set, fn);
    }
}

void FuncPtrPass::outputMsg() {
    outputDebugMsg();
    std::map<int, std::set<const Function *>> msg;
    for (auto call : call_insts) {
        int line_n = 0;
        if (auto loc = call->getDebugLoc()) {
            line_n = loc.getLine();
        }
        auto callee = call->getCalledValue();
        auto &fset = msg[line_n];
        if (auto fn = dyn_cast<Function>(callee)) {
            fset.insert(fn);
        } else {
            const auto &possible_val = possible_values[callee];
            for (auto it = possible_val.cbegin(); it != possible_val.cend(); it++) {
                fset.insert(*it);
            }
        }
    }
    for (auto &kv : msg) {
        errs() << kv.first << " : ";
        for (auto it = kv.second.begin(); it != kv.second.end(); it++) {
            if (it != kv.second.begin()) {
                errs() << ", ";
            }
            errs() << (*it)->getName();
        }
        errs() << '\n';
    }
}

#ifdef DEBUG_OUTPUT
void FuncPtrPass::outputDebugMsg() {
    debug() << "---------------- possible value of funtion pointers ----------------\n";
    for (auto &kv : dependencies) {
        auto var = kv.first;
        debugPrintVar(var) << ':';
        for (auto value : possible_values[var]) {
            debug() << ' ' << value->getName();
        }
        debug() << '\n';
    }
    debug() << "---------------- call insts ----------------\n";
    for (auto call : call_insts) {
        if (auto loc = call->getDebugLoc()) {
            debug() << loc.getLine();
        } else {
            debug() << "?";
        }
        debug() << ": ";
        if (call->hasName()) {
            debug() << call->getName();
        } else {
            debug() << "<no name>";
        }
        debug() << '\n';
    }
    debug() << "--------------------------------\n";
}
#endif