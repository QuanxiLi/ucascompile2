#include "debug.h"
#include "llvm/IR/DerivedTypes.h"
#include "llvm/IR/InstrTypes.h"
#include "llvm/IR/IntrinsicInst.h"
#include "llvm/IR/Module.h"
#include "llvm/Pass.h"
#include "llvm/Support/raw_ostream.h"
#include <deque>
#include <set>
#include <unordered_map>
#include <unordered_set>

class FuncPtrPass : public llvm::ModulePass {
public:
    static char ID; // Pass identification, replacement for typeid
    FuncPtrPass() : ModulePass(ID) {}

    // this method is not thread safety
    bool runOnModule(llvm::Module &module) override;

private:
    typedef std::set<const llvm::Function *> FValSet;
    typedef std::set<const llvm::Value *> VarSet;
    std::unordered_map<const llvm::Value *, FValSet> possible_values; // for function pointers
    std::unordered_map<const llvm::Value *, VarSet> dependent_on;
    std::unordered_map<const llvm::Value *, VarSet> dependencies;
    std::unordered_set<const llvm::Value *> fn_with_fn_args;
    std::unordered_set<const llvm::Value *> unsolved;
    std::deque<const llvm::CallBase *> call_insts;
    std::set<const llvm::Value *> changed;

private:
    void clear() {
        possible_values.clear();
        dependent_on.clear();
        dependencies.clear();
        unsolved.clear();
        call_insts.clear();
    }

    inline void addDependency(const llvm::Value *var, const llvm::Value *dependency) {
        newVar(dependency);
        dependent_on[dependency].insert(var);
        dependencies[var].insert(dependency);
    }

    void update(const llvm::Value *var);

    void addDependencies(const llvm::Value *var);

    // seek all call instructions and initialize fp_vals, unsolved, call_insts
    void initialFpVals(const llvm::Module &module);

    void initialFpValsHelp(const llvm::Instruction &inst);

    inline bool newVar(const llvm::Value *var) {
        if (dependencies.emplace(var, VarSet()).second) {
            // meet val for the first time
            unsolved.insert(var);
            addDependencies(var);
            return true;
        }
        return false;
    }

    void bindValues(const llvm::Value *var, FValSet &val_set, const llvm::Value *dependency);

    void onChangedFnWithFnArgs(const llvm::Value *var);

    inline void onChanged(const llvm::Value *var);

    inline bool possibleCallFn(const llvm::CallBase *call_inst, const llvm::Function *fn) {
        const auto callee = call_inst->getCalledValue();
        if (auto callee_fn = llvm::dyn_cast<llvm::Function>(callee)) {
            return callee_fn == fn;
        } else {
            auto possible_callee = possible_values[callee];
            return possible_callee.find(fn) != possible_callee.end();
        }
    }

    void outputMsg();

    inline bool updatePossibleValue(const llvm::Value *var, FValSet &val_set, const llvm::Function *fn) {
        assert(&val_set == &possible_values[var]);
        if (val_set.insert(fn).second) {
            debug() << "update " << var->getName() << " <- " << fn->getName() << '\n';
            changed.insert(var);
            return true;
        } else {
            return false;
        }
    }

    inline bool updatePossibleValue(const llvm::Value *var, const llvm::Function *fn) {
        return updatePossibleValue(var, possible_values[var], fn);
    }

#ifdef DEBUG_OUTPUT
    void outputDebugMsg();
#else
    inline void outputDebugMsg() {}
#endif
};