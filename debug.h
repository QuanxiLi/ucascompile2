// #define DEBUG_OUTPUT

#ifdef DEBUG_OUTPUT

#include "llvm/Support/raw_ostream.h"

inline auto &debug() {
    return llvm::errs();
}

#else

class NullOutStream {
public:
    NullOutStream() {}
    NullOutStream(const NullOutStream &) {}
    template <typename T>
    const NullOutStream &operator<<(T &&) const {
        return *this;
    }
};

inline NullOutStream debug() {
    return NullOutStream();
}

#endif
