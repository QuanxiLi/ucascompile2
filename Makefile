all: cmake
	$(MAKE) -C build

.PHONY: cmake test

cmake: build/.cmake_is_lastest

build/.cmake_is_lastest:
	@mkdir -p build
	@cd build && cmake -DCMAKE_CXX_FLAGS="`llvm-config --cxxflags` -g" -DLLVM_DIR="`llvm-config --cmakedir`" ..
	@touch build/.cmake_is_lastest

test_cases:
	@for f in `ls assign2-tests/*.c`; do \
		clang -c -g -emit-llvm $$f -o $${f%.c}.bc; \
	done

test: all
	@for f in `ls assign2-tests/*.bc`; do \
		fname=$${f#assign2-tests/}; \
		echo "- testing $${fname%.bc}"; \
		build/llvmassignment $$f; \
		cat $${f%.bc}.c | grep //; \
	done