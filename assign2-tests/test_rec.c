typedef int func_t(void);

int a();

int b(func_t *f) {
    return f();
}

func_t *c(func_t *x) {
    return x;
}

int a() {
    func_t *x = c(a);
    x();
    return b(x);
}

int rec(int x) {
    if (x > 0) {
        return rec(x - 1);
    } else {
        return 0;
    }
}

/// 6 : a
/// 14 : c
/// 15 : b