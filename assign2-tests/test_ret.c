typedef int func_ty(int);

func_ty *function(func_ty *fn) {
    return fn;
}

int f(int x) {
    return x;
}

int caller() {
    func_ty *callee = function(f);
    return callee(3);
}

/// 12 : function
/// 13 : f