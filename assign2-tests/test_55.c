typedef int F(void);

int one() {
    return 1;
}

int zero() {
    return 0;
}

int func() {
    F *f = zero;
    int s = 0;
    for (int i = 0; i < 10; i++) {
        s += i;
    }
    if (s == 55) {
        f = one;
    }
    return f();
}
